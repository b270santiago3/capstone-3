const toursData = [
	{
		id: "tour01",
		name: "El Nido",
		description: "El Nido, in Palawan Island, is a pathway to paradise islands and beaches in the Philippines. It lives up to the motto: the paradise is here!",
		price: 2499,
		onOffer: true,
    	images: [
      		"https://s1.it.atcdn.net/wp-content/uploads/2013/01/El-Nido-Philippines-PALAWAN-800x600.jpg",
      		"https://www.elnidoparadise.com/wp-content/uploads/booking-7-wonders-of-el-nido-tours.jpg",
      		"https://www.ayalaland.com.ph/app/uploads/2020/10/Picture1c.jpg",
      		"https://www.elnidoparadise.com/wp-content/uploads/booking-el-nido-tour-a.jpeg",
      		"https://media.istockphoto.com/id/1202495772/photo/aerial-view-of-hidden-beach-in-matinloc-island-el-nido-palawan-philippines-tour-c-route.jpg?s=612x612&w=0&k=20&c=9SZcU6lROeeIvZznltQCmF18yXgewaOtGUrlxZjOw9Q="
    ]
	},
	{
		id: "tour02",
		name: "Cebu",
		description: "It is one of the country's largest cities and is a bustling port. Its harbour is provided by the sheltered strait between Mactan Island and the coast. The country's oldest settlement, it is also one of its most historic and retains much of the flavour of its long Spanish heritage.",
		price: 3999,
		onOffer: true,
		images: [
		  	"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1a/5d/ef/44/caption.jpg?w=500&h=400&s=1",
		  	"https://a.cdn-hotels.com/gdcs/production163/d1946/2fbfc6ee-d61c-404b-81ff-e7f5f7474af8.jpg?impolicy=fcrop&w=800&h=533&q=medium",
		  	"https://www.planetware.com/wpimages/2022/11/philippines-cebu-best-resorts-beach-and-boats.jpg",
		  	"https://cdn.southeastasiabackpacker.com/wp-content/uploads/2017/05/Fort-San-Pedro-Cebu.jpg",
		  	"https://mycebu.ph/wp-content/uploads/2012/08/lapu-lapu.jpg"
	]
	},
	{
		id: "tour03",
		name: "Siargao",
		description: "Is an island of nine municipalities in the province of Surigao del Norte. Known as the “Surfing Capital of the Philippines”, Siargao is mainly responsible for introducing surfing to the country. Apart from surfing, Siargao is also open to other activities such as cave explorations and rock climbing.",
		price: 4599,
		onOffer: true,
		images: [
		  	"https://i0.wp.com/backpackingwithabook.com/wp-content/uploads/2022/02/guyam-island-siargao-travel-guide-1-1.png?resize=940%2C480&ssl=1",
		  	"https://media.istockphoto.com/id/93173584/photo/cloud-nine-morning.jpg?s=612x612&w=0&k=20&c=xewbUwXR8esQR-rEDnbGOY_I8TPSC-gvfHGMNdpMkDE=",
		  	"https://www.thetravellingpinoys.com/wp-content/uploads/2020/02/pxfuel-siargao-water-beach-house.jpg",
		  	"https://www.mindanews.com/wp-content/uploads/2022/10/18marama_web.jpg",
		  	"https://dailytravelpill.com/wp-content/uploads/2020/11/coconut-tree-road-siargao-aurelia-teslaru.jpg"
	]
	},
	{
		id: "tour04",
		name: "Bohol",
		description: "The home of the famous Chocolate Hills, Bohol is one of the most visited destinations in the Central Visayas region of the Philippines. The island province offers breathtaking spots for history buffs, beach lovers, and adrenaline junkies",
		price: 2899,
		onOffer: true,
		images: [
		  	"https://www.cruisemapper.com/images/ports/480-e8324d98230d.jpg",
		  	"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMzyKnRDCon4R1ldtRO7TDLPVPaS0m5zL4Gg&usqp=CAU",
		  	"https://www.travelsewhere.net/wp-content/uploads/2020/01/DSC_0886-4-1.jpg",
		  	"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTTUlVfwCFbnBcI4OmxS3c6ywd1vz9x5cfNg&usqp=CAU",
		  	"https://www.travellingking.com/wp-content/uploads/2022/12/Bohol-Tropical-beach-at-Panglao-Bohol-island-with-chairs-on-the-white-sand-beach-with-blue-sky-and-palm-trees.-.jpg"
	]
	},
	{
		id: "tour05",
		name: "Coron",
		description: "Is one of Palawan’s most popular beach and island destinations. This paradise-like getaway comprises of the eastern half of Busuanga Island, Coron Island, and 50 other islets within the vicinity, all of which are separate from the main Palawan island",
		price: 4699,
		onOffer: true,
		images: [
		  	"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0f/24/74/3e/kayangan-lake.jpg?w=700&h=500&s=1",
		  	"https://www.journeyera.com/wp-content/uploads/2016/11/coron-palawan-image-05155.jpg",
		  	"https://go2ph.club/wp-content/uploads/sites/2/2022/09/Bulog-Dos-Island-in-Coron-03.jpg",
		  	"https://content.api.news/v3/images/bin/097b335c4250fc5598d81deab1c66dda",
		  	"https://www.journeyera.com/wp-content/uploads/2019/05/taraw-peak-cliff-el-nido-5648124A5648.jpg"
	]
	},
	{
		id: "tour06",
		name: "Bicol",
		description: "Bicol is a destination suitable for all with its historical sites, eco-tourism and watersports. There is also the famous Mayon Volcano, Bulusan Volcano Natural Park, and numerous stunning waterfalls",
		price: 5999,
		onOffer: true,
		images: [
		  	"https://i0.wp.com/outoftownblog.com/wp-content/uploads/2013/09/Mayon-Lava-Trail-ATV-Adventure.jpg?resize=600%2C415",
		  	"https://4.bp.blogspot.com/-CqRFBaSFmyQ/WMS4PMXIxgI/AAAAAAAAIPE/GiRLl7ClRkgdlR_KdPXU595SqvrcaNxHACLcB/s1600/Calaguas%2BIsland-min.jpg",
		  	"https://ntolzimri.files.wordpress.com/2015/08/img_5939.jpg",
		  	"https://1.bp.blogspot.com/-ZTYYAi_2dN0/Xie5uXMfERI/AAAAAAAA66M/WIL6Jci5Q3ELkH0Fv1mFQ63z0ukA7pb5QCNcBGAsYHQ/s16000/DSC_1630.jpg",
		  	"https://upload.wikimedia.org/wikipedia/commons/5/54/Cagsawa_ruins.jpg"
	]
	},
	{
		id: "tour07",
		name: "Ilocos",
		description: "Ilocos Region is blessed with a rich topography full of man-made natural wonders inviting you to explore the best of land, air, and water. Whether it’s marveling at the Kapurpurawan Rock Formations, exploring the majestic Bangui windmills, or walking along the pebbled beaches of Pagudpud, Ilocos is sure to amaze you with its stunning landscapes",
		price: 5999,
		onOffer: true,
		images: [
		  	"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTr3KzdUQ7lSB3xa9BHH4JOSeoVg1zen5qhmMqFmq8Srt1K5b94mAwa9cposFWWtqJSSS0&usqp=CAU",
		  	"https://jontotheworld.com/wp-content/uploads/2016/08/Patapat_Viaduct_in_Pagudpud.jpg",
		  	"https://files01.pna.gov.ph/ograph/2021/09/16/img5997.jpg",
		  	"https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Calle_Crisologo%2C_Vigan%2C_Philippines_%2850025182191%29.jpg/640px-Calle_Crisologo%2C_Vigan%2C_Philippines_%2850025182191%29.jpg",
		  	"https://shoestringdiary.files.wordpress.com/2019/01/baluarte07ss_diaries.jpg?w=1366"
	]
	}

]

export default toursData;
