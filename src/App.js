import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Banner from './components/Banner';
import AdminDashboard from './components/AdminDashboard';
import Tours from './pages/Tours';
import ToursView from './pages/ToursView';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import TourDetails from './pages/TourDetails';
import AddToCart from './pages/AddToCart';
import { UserProvider } from './UserContext';
import Footer from './components/Footer';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);

  useEffect(() => {
    fetch("http://localhost:4000/user/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data._id !== undefined) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        } else {
          setUser({
            id: null,
            isAdmin: null
          });
        }
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser }}>
      <Router>
        <div>
          <AppNavbar />
          <div className="container mt-4">
            <Routes>
              <Route exact path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/tours" element={<Tours />} />
              <Route path="/tours/:tourId" element={<ToursView />} />
              <Route path="/cart" element={<AddToCart />} /> 
              <Route path="/logout" element={<Logout unsetUser={unsetUser} />} />
              <Route path="/dashboard" element={<AdminDashboard />} />
              <Route path="/tour/:id" element={<TourDetails />} />
            </Routes>
          </div>
          <Footer /> 
        </div>
      </Router>
    </UserProvider>
  );
}

export default App;