import React from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import './Highlights.css';

export default function Highlights() {
  const images = [
    'https://www.journeyera.com/wp-content/uploads/2016/11/what-to-do-in-coron-04908.jpg',
    'https://res.klook.com/image/upload/Mobile/City/domv31ccrsr0cco9tgqt.jpg',
    'https://content.shopback.com/ph/wp-content/uploads/2018/04/20074733/Cagsawaruins.jpg',
    'https://vientodelmar.com/wp-content/uploads/2014/06/shutterstock_179829473-compressed-pano.jpg',
    'https://i.redd.it/03zfkkno21a91.jpg',
  ];

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
      partialVisibilityGutter: 40,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
      partialVisibilityGutter: 30,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      partialVisibilityGutter: 30,
    },
  };

  return (
    <div>
      <Carousel responsive={responsive}>
        {images.map((imageUrl, index) => (
          <div key={index} className="slide-item">
            <img className="slide-image" src={imageUrl} alt={`Slide ${index + 1}`} />
          </div>
        ))}
      </Carousel>

    </div>
  );
}
