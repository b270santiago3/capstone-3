import React from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './TourCard.css';

export default function TourCard({ tour, showBookButton }) {
  const { id, name, description, price, onOffer } = tour;

  return (
    <div className="tour-card">
      <Card >
        <Card.Body className="text-center ">
          <Card.Title>{name}</Card.Title>
          <Card.Text>{description}</Card.Text>
          <Card.Text>Price: PhP {price}</Card.Text>
          {onOffer && <Card.Text>On Offer</Card.Text>}
          <Button variant="primary" as={Link} to={`/tour/${id}`}>
            Details
          </Button>
          {showBookButton && (
            <Button variant="success" as={Link} to={`/tour/${id}/book`}>
              Book Now
            </Button>
          )}
        </Card.Body>
      </Card>
    </div>
  );
}