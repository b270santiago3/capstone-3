import React, { useContext, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { UserContext, UserProvider } from '../UserContext';
import './AppNavbar.css';

export default function AppNavbar() {
  const { user, setUser } = useContext(UserContext);
  const [currentPage, setCurrentPage] = useState('');

  const handleLogout = () => {
    setUser(null);
    localStorage.removeItem('token');
    setCurrentPage('/tours');
  };

  const handleNavLinkClick = (page) => {
    setCurrentPage(page);
  };

  return (
    <nav className="navbar">
      <div className="nav-container">
        <NavLink exact to="/" className="navbar-brand">
          <span className="logo-text">Ganda Pilipinas Travel and Tours</span>
        </NavLink>

        <ul className="nav-menu">
          <li className="nav-item">
            <NavLink
              exact
              to="/"
              activeClassName="active"
              className="nav-links"
              onClick={() => handleNavLinkClick('/')}
            >
              Home
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              exact
              to="/tours"
              activeClassName="active"
              className="nav-links"
              onClick={() => handleNavLinkClick('/tours')}
            >
              Tours
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink
              exact
              to="/cart"
              activeClassName="active"
              className="nav-links"
              onClick={() => handleNavLinkClick('/cart')}
            >
              Bookings
            </NavLink>
          </li>
          {user ? (
            <li className="nav-item">
              <NavLink
                exact
                to="/"
                activeClassName="active"
                className="nav-links"
                onClick={handleLogout}
              >
                Logout
              </NavLink>
            </li>
          ) : (
            <>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/login"
                  activeClassName="active"
                  className="nav-links"
                  onClick={() => handleNavLinkClick('/login')}
                >
                  Login
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/register"
                  activeClassName="active"
                  className="nav-links"
                  onClick={() => handleNavLinkClick('/register')}
                >
                  Register
                </NavLink>
              </li>
            </>
          )}
        </ul>
      </div>
    </nav>
  );
}
