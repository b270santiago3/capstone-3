import React from 'react';

export default function ImageList({ images }) {
  return (
    <div className="image-list">
      {images.map((image, index) => (
        <img key={index} src={image} alt={`Image ${index}`} className="image-item" />
      ))}
    </div>
  );
}




