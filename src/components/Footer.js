import React from 'react';
import { CDBContainer, CDBBox, CDBBtn, CDBIcon } from 'cdbreact';
import './Footer.css'; // Import your CSS file for the footer styles

export default function Footer() {
  return (
    <footer className="footer shadow">
      <CDBContainer className="footer-bg">
        <CDBBox display="flex" justifyContent="between" className="py-4">
          <CDBBox className="footer-logo">
            <a href="/" className="d-flex align-items-center p-0 text-dark">
              <img alt="logo" src="https://www.lamudi.com.ph/journal/wp-content/uploads/2019/07/TQS-1.png" />
              <span className="ml-2"></span>
            </a>
          </CDBBox>
          <CDBBox display="flex" className="mt-4" justifyContent="between">
            <small className="footer-text">&copy; Ganda Pilipinas 2023, all rights reserved</small>
            <CDBBox display="flex" className="footer-social-btns">
              <CDBBtn flat color="dark" className="p-2">
                <CDBIcon fab icon="facebook-f" />
              </CDBBtn>
              <CDBBtn flat color="dark" className="mx-3 p-2">
                <CDBIcon fab icon="twitter" />
              </CDBBtn>
              <CDBBtn flat color="dark" className="p-2">
                <CDBIcon fab icon="instagram" />
              </CDBBtn>
            </CDBBox>
          </CDBBox>
        </CDBBox>
      </CDBContainer>
    </footer>
  );
}



