import React from 'react';
import TourCard from '../components/TourCard';
import toursData from '../data/toursData';
import './Tours.css';

export default function Tours() {
  return (
    <div>
      <h2 className="text-center custom-heading pt-5">The world is a book and those who do not travel read only one page so choose your poison</h2>
      {toursData.map((tour) => (
        <TourCard key={tour.id} tour={tour} />
      ))}
    </div>
  );
}