import React, { useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';
import toursData from '../data/toursData';
import { Card, Button, CardGroup } from 'react-bootstrap';

export default function ToursView() {
  const { id } = useParams();
  const [tourDetails, setTourDetails] = useState(null);

  useEffect(() => {
    const fetchedTour = toursData.find((tour) => tour.id === id);
    setTourDetails(fetchedTour);
  }, [id]);

  if (!tourDetails) {
    return <div>Loading...</div>;
  }

  const { name, description, price, onOffer, images } = tourDetails;

  const detailsContainerStyle = {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    textAlign: 'center',
    padding: '20px',
    marginBottom: '20px',
  };

  const imageContainerStyle = {
    display: 'flex',
    justifyContent: 'center',
  };

  const cardStyle = {
    margin: '10px',
    flex: '1',
  };

  const imageStyle = {
    objectFit: 'cover',
    height: '200px',
    width: '100%',
  };

  const buttonContainerStyle = {
    textAlign: 'center',
    paddingTop: '10px',
  };

  return (
    <div>
      <div style={detailsContainerStyle}>
        <h3>{name}</h3>
        <p>{description}</p>
        <p>Price: PhP {price}</p>
        {onOffer && <p>On Offer</p>}
      </div>
      <div style={imageContainerStyle}>
        <CardGroup>
          {images.map((image, index) => (
            <Card key={index} style={cardStyle}>
              <Card.Img src={image} style={imageStyle} />
            </Card>
          ))}
        </CardGroup>
      </div>
      <div style={buttonContainerStyle}>
        <Link to="/cart">
          <Button variant="primary" style={{ fontSize: '16px', padding: '8px 20px' }}>
            Book
          </Button>
        </Link>
      </div>
    </div>
  );
}