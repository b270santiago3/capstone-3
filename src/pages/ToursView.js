import React, { useContext } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddToCart({ tour }) {
  const { user } = useContext(UserContext);

  const enroll = (tourId) => {
    fetch(`${process.env.REACT_APP_API_URL}/user/book`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        tourId: tourId
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if (data) {
        Swal.fire({
          title: "Successfully booked!",
          icon: "success",
          text: "You have successfully booked for this tour."
        });
      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        });
      }
    });
  };

  return (
    <Card>
      <Card.Body className="text-center">
        <Card.Title>{tour && tour.name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{tour && tour.description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {tour && tour.price}</Card.Text>
        <Card.Subtitle>Class Schedule</Card.Subtitle>
        <Card.Text>8 am - 5 pm</Card.Text>
        {user.id !== null ? (
          <Button variant="primary" onClick={() => enroll(tour && tour.tourId)}>Enroll</Button>
        ) : (
          <Button variant="danger" as={Link} to="/login">
            Log in to Enroll
          </Button>
        )}
      </Card.Body>
    </Card>
  );
}
